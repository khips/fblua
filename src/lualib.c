#include "lualib.h"
#include "msgparse.h"

#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>
#include "lualib.h"

// Read program in from a prop list:
typedef struct prop_reader_status {
	dbref player;
	dbref what;
	const char *listname;
	int position;
	int mesgtyp;
	int addspace;
} prop_reader;

const char *read_next_prop(lua_State *L, void *data, size_t *size) {
	prop_reader* stat = data;

	if (stat->addspace) {
		*size = 1;
		stat->addspace = 0;

		return "\n";
	}

	// Read the next line
	const char *propval;
	int blessed;


	propval = get_list_item(stat->player, stat->what, OWNER(stat->what),
							stat->listname, stat->position, stat->mesgtyp,
							&blessed);
	// Oof.
	*size = strlen(propval);

	stat->position++;
	stat->addspace = 1;

	return propval;
}



static int run_checks(lua_State *L, char *checklist) {
	// Run checks against the values on the stack.
	// Checklist contains an arbitrary number of characters, each
	// corresponding to a check.  Checks are separated by commas.
	// Will change v
	// 
	// run_checks(L, "t,s,i")  <-- 

	// If run_checks encounters an error, it returns a non-zero
	// value and pushes a string to the stack describing the
	// error.

	// Incomplete.
	return 0;

	int arg = 0;
	int top = lua_gettop(L);
	char *p = checklist;
	int optional = 0;

	while (p++) {
		switch(*p) {
			case ',':
				arg++;
				//if (arg
				break;
			default:
				break;
		}
	}
}

static dbref fblua_todbref(lua_State  *L, int index) {
	dbref object = lua_tointeger(L, index);

}

/// Fuzzball module:
static int fblua_notify(lua_State *L) {
	const char* message;

	if (run_checks(L, "{o},s")) {
		lua_error(L);
	}

	message = lua_tostring(L, 2);
	dbref player;

	if (lua_istable(L, 1)) {
		lua_pushnil(L);
		while (lua_next(L, 1)) {
			player = lua_tointeger(L, -1);
			notify(player, message);
			lua_pop(L, 1);
		}
	} else if (lua_isnumber(L, 1)) {
		player = lua_tointeger(L, 1);
		notify(player, message);
	} else {
		// ERROR!
	}

	return 0;
}

static int fblua_name(lua_State *L) {
	dbref object = fblua_todbref(L, 1);

	lua_pushstring(L, NAME(object));

	return 1;
}

static int 
fblua_property(lua_State *L) {
	dbref player = lua_tointeger(L, FB_PLAYER_IDX);
	dbref object = fblua_todbref(L, 1);
	const char *propname = lua_tostring(L, 2);

	const char *propval;
	int blessed;
	
	propval = safegetprop(player, object, object, propname, 0, &blessed);
	lua_pushstring(L, propval);

	return 1;
}

static int
fblua_location(lua_State *L) {
	dbref object = fblua_todbref(L, 1);

	lua_pushinteger(L, DBFETCH(object)->location);
	return 1;
}

static int fblua_match(lua_State *L) {
	struct match_data md;
	int descr = lua_tointeger(L, FB_DESCRIPTOR_IDX);
	int args = lua_gettop(L);
	const char *matchstr;
	dbref player, what;
	int remote = args == 2;

	player = lua_tointeger(L, FB_PLAYER_IDX);
	matchstr = lua_tostring(L, remote ? 2 : 1);
	
	if (remote) {
		what = lua_tointeger(L, 1);
		init_match_remote(descr, player, what, matchstr, NOTYPE, &md);
	} else {
		init_match(descr, player, matchstr, NOTYPE, &md);
	}

	match_absolute(&md);
	match_all_exits(&md);
	match_neighbor(&md);
	match_possession(&md);
	match_registered(&md);

	lua_pushinteger(L, match_result(&md));

	return 1;
}

// Replacement print() function.
static int fblua_print(lua_State *L) {
	// The executor is stored at upvalue 1.  Display the argument to
	// that executor.
	lua_pushvalue(L, lua_upvalueindex(1));
	lua_insert(L, 1);

	return fblua_notify(L);
}

static int fblua_sleep(lua_State *L) {
	// Remove the running coroutine from operation and place it on
	// the queue.
	return lua_yield(L, 1);
}

static int fblua_force(lua_State *L) {
	// TODO: Not implemented
	dbref object = fblua_todbref(L, 1);
	const char *cmd = lua_tostring(L, 2);

	return 0;
}

static int fblua_newthing(lua_State *L) {
	// Create a object with type Thing and return its dbref on the
	// stack.  Takes the name of the new object as the first argument
	// and its location as the second.

	// TODO: Not implemented

	return 1;
}

static int fblua_online(lua_State *L) {
	int count = pcount();
	int i = 1;

	lua_newtable(L);

	for (i = 1; i <= count; i++) {
		lua_pushinteger(L, i);
		lua_pushinteger(L, pdbref(i));
		lua_settable(L, -3);
	}

	return 1;
}

static int fblua_type(lua_State *L) {
	dbref object = fblua_todbref(L, 1);
	const char *type;

	switch (Typeof(object)) {
		case TYPE_PLAYER:
			type = "player";
			break;
		case TYPE_ROOM:
			type = "room";
			break;
		case TYPE_EXIT:
			type = "exit";
			break;
		case TYPE_THING:
			type = "thing";
			break;
		case TYPE_PROGRAM:
			type = "program";
			break;
		default:
			type = "bad";
			break;
	}

	lua_pushstring(L, type);

	return 1;
}

static int
fblua_dbtop(lua_State *L) {
	lua_pushinteger(L, db_top);
	return 1;
}

// Helper for contents() and exits().
// Models how functions should work when they return lists.
// Could be subject to race conditions, but probably not an
// important consideration for our purposes.
static int fblua_iter_llist(lua_State *L) {
	dbref object = lua_tointeger(L, lua_upvalueindex(1));

	if (object == NOTHING)
		return 0;

	// Return value:
	lua_pushinteger(L, object);

	// Advance the upvalue:
	object = DBFETCH(object)->next;
	lua_pushinteger(L, object);
	lua_replace(L, lua_upvalueindex(1));

	return 1;
}

static int 
fblua_contents(lua_State *L) {
	dbref object = fblua_todbref(L, 1);
	
	object = DBFETCH(object)->contents;
	lua_pushinteger(L, object);
	lua_pushcclosure(L, fblua_iter_llist, 1);

	return 1;
}

static int 
fblua_exits(lua_State *L) {
	dbref object = fblua_todbref(L, 1);

	switch(Typeof(object)) {
		case TYPE_ROOM:
		case TYPE_THING:
		case TYPE_PLAYER:
			object = DBFETCH(object)->exits;
			break;
		default:
			object = NOTHING;
	}

	lua_pushinteger(L, object);
	lua_pushcclosure(L, fblua_iter_llist, 1);

	return 1;
}

static int
fblua_links(lua_State *L) {
	dbref object = fblua_todbref(L, 1);
	int count = DBFETCH(object)->sp.exit.ndest;
	int i;

	lua_newtable(L);

	switch (Typeof(object)) {
		case TYPE_ROOM:
			object = DBFETCH(object)->sp.room.dropto;
			break;
		case TYPE_PLAYER:
			object = PLAYER_HOME(object);
			break;
		case TYPE_THING:
			object = THING_HOME(object);
			break;
		case TYPE_EXIT:
			// add all links to table
			for (i = 0; i < count; i++) {
				lua_pushinteger(L, i+1);
				lua_pushinteger(L, DBFETCH(object)->sp.exit.dest[i]);
				lua_settable(L, -3);
			}
			// return immediately.
			return 1;
		case TYPE_PROGRAM:
		default:
			object = -1;
			break;
	}

	if (object >= 0) {
		lua_pushinteger(L, 1);
		lua_pushinteger(L, object);
		lua_settable(L, -3);
	}

	return 1;
}


static int 
fblua_loadfns(lua_State *L) {
	// Load functions from another program object.
	dbref program = fblua_todbref(L, 1);
	lua_pop(L, 1);

	if (Typeof(program) != TYPE_LUA) {
		lua_pushstring(L, "Attempting to include non-Lua object.");
		lua_error(L);
	}

	// Check permissions
	dbref thisprog = lua_tointeger(L, FB_PROG_IDX);
	dbref player = lua_tointeger(L, FB_PLAYER_IDX);
	dbref perms = OWNER(thisprog);

	if (TrueWizard(perms) || Linkable(program) ||
			perms == OWNER(program)) {
		struct prop_reader_status stat = {
			player, program, "program", 1, 0, 0
		};

		int compiles = lua_load(L, read_next_prop, (void*)&stat, "program", "t");

		if (compiles == LUA_OK) {
			// Chunk environment:
			lua_newtable(L);

			// Metatable for chunk environment:
			lua_newtable(L);
			lua_getglobal(L, "_G");
			lua_setfield(L, -2, "__index");

			// Set environment metatable.
			lua_setmetatable(L, -2);
			// Copy it.
			lua_pushvalue(L, -1);
			lua_setupvalue(L, -3, 1);
			// Move the copy of the environment below the function.
			lua_insert(L, 1);
			// Call the chunk:
			// TODO: prevent recursive calls
			lua_call(L, 0, 0);

			// returns the environment after being altered by the
			// called program
			return 1;
		}
		
		lua_pop(L, 1);

		// Report the error:
		lua_pushstring(L, "Called program not compilable: ");
		lua_insert(L, 1);
		lua_concat(L, 2);
		lua_error(L);
	}

	lua_pushstring(L, "Permission denied.");
	lua_error(L);

	return 0;
}

static const struct luaL_Reg fuzzball_lib_f[] = {
	// TODO: {"dig", fblua_dig},
	{"contents", fblua_contents},
	// TODO: {"created", fblua_created},
	{"dbtop", fblua_dbtop},
	{"exits", fblua_exits},
	{"links", fblua_links},
	{"load", fblua_loadfns},
	{"location", fblua_location},
	{"match", fblua_match},
	{"name", fblua_name},

	// creation functions:
	// TODO: {"newthing", fblua_newthing},
	// TODO: {"newroom", fblua_newroom},
	// TODO: {"newexit", fblua_newexit},

	{"notify", fblua_notify},
	{"online", fblua_online},
	{"prop", fblua_property},
	{"sleep", fblua_sleep},
	//TODO: {"setprop", fblua_setprop},
	//TODO: {"testlock", fblua_testlock},
	{"type", fblua_type},
	{NULL, NULL}
};


// Count hook:
void fblua_count_hook(lua_State *L, lua_Debug *ar) {
	// Interrupt execution.
	lua_yield(L, 0);
}

void fblua_maxinst_hook(lua_State *L, lua_Debug *ar) {
	lua_pushstring(L, "Maximum instruction count reached.");
	lua_error(L);
}

// Manage lua_State lifetime.
int
fblua_ref(lua_State *L, int change) {
	// Decrement the reference count of the lua_State.
	// Definitely not the best way to do this, but it'll stand
	// for now.
	// Returns the new reference count, or 0 if the refcount is
	// less than or equal to 0.

	char buf[10];
	int rcount;

	lua_pushstring(L, "fb_refcount");
	lua_pushstring(L, "fb_refcount");
	lua_gettable(L, LUA_REGISTRYINDEX);

	lua_pushinteger(L, change);
	lua_arith(L, LUA_OPADD);
	rcount = lua_tointeger(L, -1);

	if (rcount <= 0) {
		lua_close(L);
		return 0;
	}
	lua_settable(L, LUA_REGISTRYINDEX);
	return rcount;
}
//////////////

int call_lua_prog(dbref player, dbref prog, int descr, 
				dbref trig, const char *arg) {

	lua_State *L = luaL_newstate();

	// REFCOUNT:
	lua_pushstring(L, "fb_refcount");
	lua_pushinteger(L, 1);	// initial refcount of 1
	lua_settable(L, LUA_REGISTRYINDEX);

	// ENVIRONMENT SETUP:
	lua_sethook(L, fblua_count_hook, LUA_MASKCOUNT, 500000);
	//lua_sethook(L, fblua_maxinst_hook, LUA_MASKCOUNT, 1000000);

	// Modules available 
	luaL_requiref(L, "_G", luaopen_base, 1);
	luaL_requiref(L, "table", luaopen_table, 1);
	luaL_requiref(L, "string", luaopen_string, 1);
	luaL_requiref(L, "math", luaopen_math, 1);
	lua_pop(L, 4); // pop the modules

	lua_newtable(L);
	// upvalues for the library functions:
	lua_pushinteger(L, player);	// 1: executor
	lua_pushinteger(L, prog);	// 2: program
	lua_pushinteger(L, descr);	// 3: descriptor
	lua_pushinteger(L, trig);	// 4: triggering action

	luaL_setfuncs(L, fuzzball_lib_f, 4);
	lua_pushvalue(L, -1);
	lua_setglobal(L, "fuzzball");

	lua_pushinteger(L, player);
	lua_setfield(L, -2, "executor");

	lua_pushstring(L, arg);
	lua_setfield(L, -2, "arg");

	// print() function
	lua_pushinteger(L, player);
	lua_pushcclosure(L, fblua_print, 1);
	lua_setglobal(L, "print");
	// DONE SETUP

	// LOAD PROGRAM TEXT:
	struct prop_reader_status stat = {
		player, prog, "program", 1, 0, 0
	};

	int compiles = lua_load(L, read_next_prop, (void*)&stat, "program", "t");
	if (compiles == LUA_OK) {
		//int stat = lua_pcall(L, 0, 0, 0);
		int delay = 0;
		int stat = lua_resume(L, NULL, 0);
		int scount = lua_gettop(L);

		switch(stat) {
			case LUA_YIELD:
				// Store the state on the timequeue to continue
				// running later.
				if (scount == 1) {
					delay = lua_tointeger(L, -1);
					lua_pop(L, 1);
				}

				add_lua_event(L, delay, descr, player, getloc(player),
						trig, prog, "cmd", arg);
				break;
				
			case LUA_ERRRUN:
			case LUA_ERRMEM:
			case LUA_ERRERR:
			case LUA_ERRGCMM:
				notify_nolisten(player, "Error while running lua program:", 1);
				notify_nolisten(player, lua_tostring(L, -1), 1);
			case LUA_OK:
				break;

		}

		fblua_ref(L, -1);
	} else {
		notify(player, "Error while compiling lua.");
		notify(player, lua_tostring(L, -1));
	}

	return 0;
}

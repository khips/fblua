#include "externs.h"

#define FB_PLAYER_IDX lua_upvalueindex(1)
#define FB_PROG_IDX lua_upvalueindex(2)
#define FB_DESCRIPTOR_IDX lua_upvalueindex(3)
#define FB_TRIG_IDX lua_upvalueindex(4)

int call_lua_prog(dbref player, dbref prog, int descr, 
				dbref trig, const char *arg);
